# Sisop Praktikum Modul 1 2023 FD IT25

## Soal 1
**university_survey.sh**

```
#Soal A
awk -F '("[^"]+")?,' '/Japan/ {print $1,"`"$2"`"} NR==1{print $1,$2}' 2023UnivRank.csv | sort -n -k 1 | head -6
echo \
#Soal B
awk -F '("[^"]+")?,' '/Japan/ {print $1,"`"$2"`",$9} NR==1{print $1,$2,"`"$9"`"}' 2023UnivRank.csv | sort -t '`' -n -k 3 | head -6
echo \
#Soal C
awk -F '("[^"]+")?,' '/Japan/ {print $1,"`"$2"`",$20} NR==1{print $1,$2,"`"$20"`"}' 2023UnivRank.csv | sort -t '`' -n -k 3 | head -11
echo \
#Soal D
awk -F '("[^"]+")?,' '/Keren/ {print $1,"`"$2"`"} NR==1{print $1,$2}' 2023UnivRank.csv
```

### -Penjelasan-

> echo \
- [ ] mencetak baris baru yang kosong sebagai pemisah

#### Soal A
> awk
- [ ] menjalankan perintah awk
> -F '("[^"]+")?,' 
- [ ] sebagai pemisah antar kolom, yaitu koma (,) namun abaikan tanda koma di dalam teks yang diapit tanda petik dua (")
> '/Japan/ {print $1,""$2""}'
- [ ] temukan baris yang mengandung kata "Japan" kemudian cetak kolom pertama dan kedua, namun apit nilai kolom kedua dengan tanda kutip (")
> NR==1{print $1,$2}
- [ ] mencetak baris pertama (header nya) namun cetak kolom pertama dan kedua saja
> 2023UnivRank.csv
- [ ] target file yang akan dieksekusi oleh awk
> | sort -n -k 1 
- [ ] mengurutkan secara numerik dan -k 1 untuk mengurutkan berdasarkan kolom pertama
> | head -6
- [ ] membatasi print agar sampai baris 6 teratas. 6 teratas karena header juga di print. Sisanya, yaitu 5 untuk baris data

#### Soal B
> awk
- [ ] menjalankan perintah awk
> -F '("[^"]+")?,' 
- [ ] sebagai pemisah antar kolom, yaitu koma (,) namun abaikan tanda koma di dalam teks yang diapit tanda petik dua (")
> '/Japan/ {print $1,"`"$2"`",$9}'
- [ ] temukan baris yang mengandung kata "Japan" kemudian cetak kolom pertama, kedua, dan ke-9, namun apit nilai kolom kedua dengan tanda kutip (")
> NR==1{print $1,$2,$9}
- [ ] untuk mencetak baris pertama (header nya) namun cetak kolom pertama, kedua, dan ke-9 saja dan apit nilai kolom ke-9 dengan tanda kutip (")
> 2023UnivRank.csv
- [ ] target file yang akan dieksekusi oleh awk
> | sort -t '`' -n -k 3
- [ ] mengurutkan secara numerik dan -k 3 untuk mengurutkan berdasarkan kolom ketiga dari hasil awk dimana tanda (`) adalah pemisahnya
> | head -6
- [ ] membatasi print agar sampai baris 6 teratas. 6 teratas karena header juga di print. Sisanya, yaitu 5 untuk baris data

#### Soal C
##### Penjelasannya mirip dengan 'Soal B', namun di 'Soal C', yang diambil adalah kolom ke-20 bukan kolom ke-9, serta yang dibatasi adalah data ke-11 teratas (head -11)

#### Soal D
> awk
- [ ] menjalankan perintah awk
> -F '("[^"]+")?,' 
- [ ] sebagai pemisah antar kolom, yaitu koma (,) namun abaikan tanda koma di dalam teks yang diapit tanda petik dua (")
> '/Keren/ {print $1,"`"$2"`"}'
- [ ] temukan baris yang mengandung kata "Keren" kemudian cetak kolom pertama dan kedua, namun apit nilai kolom kedua dengan tanda kutip (")
> NR==1{print $1,$2}
- [ ] untuk mencetak baris pertama (header nya) namun cetak kolom pertama dan kedua saja
> 2023UnivRank.csv
- [ ] target file yang akan dieksekusi oleh awk



## Soal 2
**kobeni_liburan.sh**
```
#!/bin/bash

cat << EOF > .kobeni_liburan_thegambar.sh
#!/bin/bash

# mengambil waktu saat ini (jamnya saja)
hournow=$(date +"%H")
url="https://www.google.com/search?q=wallpaper+landscape+indonesia&tbm=isch"
useragent="Mozilla/5.0 (Linux; Android 7.1.1; G8231 Build/41.2.A.0.219; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/59.0.3071.125 Mobile Safari/537.36"
src=$(curl -A "$useragent" -s "$url")

# mengecek jam (apakah 00:00 malam atau tidak)
if [ $hournow -eq 0 ]; then
  # download satu gambar saja
    img=$(echo $src | grep -o -E 'https://[^"]+\.jpg' | shuf -n 1)
    wget -O "perjalanan_1.jpg" --user-agent="$useragent" "$img"
else
  # menghitung jumlah gambar yang akan didownload
    totImg=$((hournow % 24 / 10 + 1))

  # membuat folder kumpulan
    folder="kumpulan_$((hournow / 10 + 1)).FOLDER"
    mkdir -p $folder

  # mengunduh gambar sebanyak jam
    for ((i = 1; i <= totImg; i++)); do
        img=$(echo $src | grep -o -E 'https://[^"]+\.jpg' | shuf -n 1)
        wget -O "${folder}/perjalanan_${i}.jpg" --user-agent="$useragent" "$img"
    done
fi
EOF

cat << EOF > .kobeni_liburan_thezip.sh
#!/bin/bash

# Menghitung jumlah file zip yang sudah ada
num_zip=$(ls -l devil_* | wc -l)
num_zip=$((num_zip+1))

# Men-zip folder kumpulan dengan nama "devil_NOMOR ZIP"
zip -r "devil_${num_zip}.zip" "kumpulan_${num_zip}"

# Menghapus folder kumpulan yang sudah di-zip
rm -rf "kumpulan_${num_zip}"
EOF

(crontab -l ; echo "0 */10 * * * /.kobeni_liburan_thegambar.sh") | crontab -
(crontab -l ; echo "0 0 * * *  /.kobeni_liburan_thezip.sh") | crontab -
```

### -Penjelasan-

> cat << EOF > .kobeni_liburan_thegambar.sh
- [ ] melakukan cat hingga EOF dimana setelah selesai, akan memasukkan semuanya ke file baru bernama '.kobeni_liburan_thegambar.sh'
> hournow=$(date +"%H")
- [ ] mengambil waktu saat ini (jamnya saja tapi) kemudian dimasukkan ke variabel 'hournow'
> url="https://www.google.com/search?q=wallpaper+landscape+indonesia&tbm=isch"
- [ ] membuat variabel 'url' diisi dengan link pencarian google image dengan kata kunci 'wallpaper landscape indonesia'
> useragent="Mozilla/5.0 (Linux; Andr....
- [ ] membuat variabel 'useragent' diisi dengan User Agent yang akan digunakan untuk melakukan curl dan wget agar tidak terjadi error (ToS Google)
> src=$(curl -A $useragent -s "$url")
- [ ] melakukan curl terhadap variabel 'url' dengan useragent variabel 'useragent' dan menyimpannya ke dalam variabel 'src'
> if [ $hournow -eq 0 ]; then
- [ ] mengecek jam/waktu saat ini (apakah 00:00 malam atau tidak)
> img=$(echo $src | grep -o -E 'https://[^"]+\.jpg' | shuf -n 1) .... wget -O "perjalanan_1.jpg" --user-agent="$useragent" "$img"
- [ ] mengambil satu link gambar secara acak dengan shuffle dari variabel 'src' dan mengunduh gambar tersebut, lalu memberinya nama 'perjalanan_1.jpg'
> totImg=$((hournow % 24 / 10 + 1))
- [ ] menghitung jumlah gambar yang akan didownload dan memasukkannya ke variabel 'totImg'
> folder="kumpulan_$((hournow / 10 + 1)).FOLDER" .... mkdir -p $folder
- [ ] membuat folder kumpulan
> for ((i = 1; i <= totImg; i++)); do .... done
- [ ] mengunduh gambar sebanyak jam
> cat << EOF > .kobeni_liburan_thezip.sh
- [ ] melakukan cat hingga EOF dimana setelah selesai, akan memasukkan semuanya ke file baru bernama '.kobeni_liburan_thezip.sh'
> num_zip=$(ls -l devil_* | wc -l) .... num_zip=$((num_zip+1))
- [ ] menghitung jumlah zip berawalan 'devil_' yang ada saat ini di direktori saat ini
> zip -r "devil_${num_zip}.zip" "kumpulan_${num_zip}"
- [ ] men-zip folder kumpulan dengan nama "devil_NOMOR ZIP"
> rm -rf "kumpulan_${num_zip}"
- [ ] menghapus folder kumpulan yang sudah di-zip
> (crontab -l ; echo "0 */10 * * * /.kobeni_liburan_thegambar.sh") | crontab -
- [ ] menambahkan file '.kobeni_liburan_thegambar.sh' ke crontab dan melakukan run terhadap file tersebut setiap 10 jam
> (crontab -l ; echo "0 0 * * *  /.kobeni_liburan_thezip.sh") | crontab -
- [ ] menambahkan file '.kobeni_liburan_thezip.sh' ke crontab dan melakukan run terhadap file tersebut setiap hari pukul 00:00