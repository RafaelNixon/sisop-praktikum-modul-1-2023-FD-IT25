#!/bin/bash

#Soal A
awk -F '("[^"]+")?,' '/Japan/ {print $1,"`"$2"`"} NR==1{print $1,$2}' 2023UnivRank.csv | sort -n -k 1 | head -6
echo \
#Soal B
awk -F '("[^"]+")?,' '/Japan/ {print $1,"`"$2"`",$9} NR==1{print $1,$2,"`"$9"`"}' 2023UnivRank.csv | sort -t '`' -n -k 3 | head -6
echo \
#Soal C
awk -F '("[^"]+")?,' '/Japan/ {print $1,"`"$2"`",$20} NR==1{print $1,$2,"`"$20"`"}' 2023UnivRank.csv | sort -t '`' -n -k 3 | head -11
echo \
#Soal D
awk -F '("[^"]+")?,' '/Keren/ {print $1,"`"$2"`"} NR==1{print $1,$2}' 2023UnivRank.csv
