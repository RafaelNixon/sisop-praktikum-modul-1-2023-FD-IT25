#!/bin/bash

cat << EOF > .kobeni_liburan_thegambar.sh
#!/bin/bash

# mengambil waktu saat ini (jamnya saja)
hournow="$(date +"%H")"
url="https://www.google.com/search?q=wallpaper+landscape+indonesia&tbm=isch"
useragent="Mozilla/5.0 (Linux; Android 7.1.1; G8231 Build/41.2.A.0.219; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/59.0.3071.125 Mobile Safari/537.36"
src="$(curl -A "$useragent" -s "$url")"

# mengecek jam (apakah 00:00 malam atau tidak)
if [ $hournow -eq 0 ]; then
  # download satu gambar saja
    img=$(echo $src | grep -o -E 'https://[^"]+\.jpg' | shuf -n 1)
    wget -O "perjalanan_1.jpg" --user-agent="$useragent" "$img"
else
  # menghitung jumlah gambar yang akan didownload
    totImg=$((hournow % 24 / 10 + 1))

  # membuat folder kumpulan
    folder="kumpulan_$((hournow / 10 + 1)).FOLDER"
    mkdir "$folder"

  # mengunduh gambar sebanyak jam
    for ((i = 1; i <= totImg; i++)); do
        img=$(echo $src | grep -o -E 'https://[^"]+\.jpg' | shuf -n 1)
        wget -O "${folder}/perjalanan_${i}.jpg" --user-agent="$useragent" "$img"
    done
fi
EOF

cat << EOF > .kobeni_liburan_thezip.sh
#!/bin/bash

# Menghitung jumlah file zip yang sudah ada
num_zip=$(ls -l devil_* | wc -l)
num_zip=$((num_zip+1))

# Men-zip folder kumpulan dengan nama "devil_NOMOR ZIP"
zip -r "devil_${num_zip}.zip" "kumpulan_${num_zip}"

# Menghapus folder kumpulan yang sudah di-zip
rm -rf "kumpulan_${num_zip}"
EOF

(crontab -l ; echo "0 */10 * * * .kobeni_liburan_thegambar.sh") | crontab -
(crontab -l ; echo "0 0 * * *  .kobeni_liburan_thezip.sh") | crontab -
